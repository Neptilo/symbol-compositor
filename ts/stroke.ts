export abstract class Stroke {
    toFill: boolean;

    abstract draw(ctx: CanvasRenderingContext2D, offset: number[], scale: number): void;
    abstract translate(t: number[]): void;
    abstract getCoordMin(dim: number): number;
    abstract getCoordMax(dim: number): number;
    abstract eval(dim: number, x: number): number;
    abstract diff(dim: number, x: number): number;
    abstract minDist(pt: number[], deg: number): number;
    abstract maxDist(pt: number[], deg: number): number;
    abstract scale(s: number, c: number[]): void;
}