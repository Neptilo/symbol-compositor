import { Arc } from "./arc.js";
import { Dot } from "./dot.js";
import { Line } from "./line.js";
import { Stroke } from "./stroke.js";

export class Symbol {
    strokes: Stroke[];
    coordMin: number[];
    coordMax: number[];
    nextSpacing: number;

    constructor() {
        this.strokes = [];
        this.coordMin = [Infinity, Infinity];
        this.coordMax = [-Infinity, -Infinity];

        // spacing applied when adding the next symbol after
        // and only that one
        this.nextSpacing = 0;
    }

    static bisect(f: (x: number) => number, a: number, b: number) {
        let n = 1;
        while (n <= 10) {
            const c = (a + b) / 2;
            if (f(c) == 0 || (b - a) / 2 < 0.01) {
                return c;
            }
            n++;
            if (f(c) * f(a) >= 0) {
                a = c;
            } else {
                b = c;
            }
        }
        console.error("Bisection failed. Max number of steps exceeded.");
    }
    
    // given two strokes representing unambiguous continuous functions in one
    // direction, returns the minimum vertical tranlation that needs
    // to be applied to the second one so that they don't overlap
    static getMinimumTranslationUnambiguous(
        firstStroke: Stroke,
        secondStroke: Stroke,
        dim: number)
    {
        const codim = Number(!dim);
        const yA = Math.max(
            firstStroke.getCoordMin(codim),
            secondStroke.getCoordMin(codim));
        const yB = Math.min(
            firstStroke.getCoordMax(codim),
            secondStroke.getCoordMax(codim));
        if (yA > yB) {
            return -secondStroke.getCoordMin(dim);
        } else {
            const df = function(x: number) {
                return secondStroke.diff(dim, x) - firstStroke.diff(dim, x);
            };
            if (yA != yB && df(yA) < 0 && df(yB) > 0) {
                // C is the point where the two strokes will touch
                const yC = Symbol.bisect(df, yA, yB);
                return firstStroke.eval(dim, yC) - secondStroke.eval(dim, yC);
            } else {
                return Math.max(
                    firstStroke.eval(dim, yA) - secondStroke.eval(dim, yA),
                    firstStroke.eval(dim, yB) - secondStroke.eval(dim, yB));
            }
        }
    }

    static getMaximumTranslationUnambiguous(
        firstStrokes: Stroke[], secondStrokes: Stroke[], dim: number) {
        if (firstStrokes.length && secondStrokes.length) {
            let t = -Infinity;
            for (const i in firstStrokes) {
                for (const j in secondStrokes) {
                    const d = Symbol.getMinimumTranslationUnambiguous(
                        firstStrokes[i], secondStrokes[j], dim);
                    if (d > t)
                        t = d;
                }
            }
            return t;
        } else {
            return 0;
        }
    }

    // given two strokes, returns the minimum vertical tranlation that needs
    // to be applied to the second one so that they don't overlap
    static getMinimumTranslation(
        firstStroke: Stroke,
        secondStroke: Stroke,
        dim: number)
    {
        const codim = Number(!dim);
        let firstStrokes: Stroke[], secondStrokes: Stroke[];
    
        // if strokes are arcs, split them into sub-arcs
        // if strokes are lines in the direction of dim, only take one dot
        if (firstStroke instanceof Arc) {
            firstStrokes = firstStroke.split(dim);
        } else if (
            firstStroke instanceof Line &&
            firstStroke.v0[codim] == firstStroke.v1[codim]) {
            const dotPos = [0, 0];
            dotPos[dim] = Math.max(firstStroke.v0[dim], firstStroke.v1[dim]);
            dotPos[codim] = firstStroke.v0[codim];
            firstStrokes = [new Dot(dotPos)];
        } else {
            firstStrokes = [firstStroke];
        }
        if (secondStroke instanceof Arc) {
            secondStrokes = secondStroke.split(dim);
        } else if (
            secondStroke instanceof Line &&
            secondStroke.v0[codim] == secondStroke.v1[codim]) {
            const dotPos = [0, 0];
            dotPos[dim] = Math.min(secondStroke.v0[dim], secondStroke.v1[dim]);
            dotPos[codim] = secondStroke.v0[codim];
            secondStrokes = [new Dot(dotPos)];
        } else {
            secondStrokes = [secondStroke];
        }
    
        if (firstStrokes.length == 1 && secondStrokes.length == 1) {
            return Symbol.getMinimumTranslationUnambiguous(
                firstStrokes[0], secondStrokes[0], dim);
        } else {
            return Symbol.getMaximumTranslationUnambiguous(
                firstStrokes, secondStrokes, dim);
        }
    }

    static getMaximumTranslation(
        firstStrokes: Stroke[], secondStrokes: Stroke[], dim: number)
    {
        if (firstStrokes.length && secondStrokes.length) {
            let t = -Infinity;
            for (const i in firstStrokes) {
                for (const j in secondStrokes) {
                    const d = Symbol.getMinimumTranslation(
                        firstStrokes[i], secondStrokes[j], dim);
                    if (d > t)
                        t = d;
                }
            }
            return t;
        } else {
            return 0;
        }
    }
    
    push(stroke: Stroke) {
        this.strokes.push(stroke);
        this.coordMin[0] = Math.min(this.coordMin[0], stroke.getCoordMin(0));
        this.coordMin[1] = Math.min(this.coordMin[1], stroke.getCoordMin(1));
        this.coordMax[0] = Math.max(this.coordMax[0], stroke.getCoordMax(0));
        this.coordMax[1] = Math.max(this.coordMax[1], stroke.getCoordMax(1));
    }

    getWidth() {
        return this.coordMax[0] - this.coordMin[0];
    }

    getHeight() {
        return this.coordMax[1] - this.coordMin[1];
    }

    translate(t: number[]) {
        this.coordMin[0] += t[0];
        this.coordMin[1] += t[1];
        this.coordMax[0] += t[0];
        this.coordMax[1] += t[1];
        for (const i in this.strokes)
            this.strokes[i].translate(t);
    }

    scale(s: number, c: number[]) {
        // todo update for c
        this.coordMin[0] *= s;
        this.coordMin[1] *= s;
        this.coordMax[0] *= s;
        this.coordMax[1] *= s;

        for (const i in this.strokes)
            this.strokes[i].scale(s, c);
    }

    addAfter(symbol: Symbol, dim: number) {
        if (symbol.nextSpacing != 0)
        // This is a special spacer symbol
            this.nextSpacing = symbol.nextSpacing;

        if (!symbol.strokes.length)
            return;

        const codim = Number(!dim);
        let t = [0, 0];
        if (isFinite(this.coordMin[codim]) && isFinite(this.coordMax[codim])) {
            // center symbol on main symbol's center
            t[codim] = (this.coordMin[codim] + this.coordMax[codim]
                - symbol.coordMin[codim] - symbol.coordMax[codim]) / 2;
            symbol.translate(t);
        }

        // find maximum vertical translation
        t = [0, 0];
        t[dim] = Symbol.getMaximumTranslation(this.strokes, symbol.strokes, dim) + this.nextSpacing;

        // Reset spacing now that it's been used
        this.nextSpacing = 0;

        symbol.translate(t);
        for (const i in symbol.strokes) {
            this.push(symbol.strokes[i]);
        }
    }

    // Superimpose a new symbol over the main symbol
    addOver(symbol: Symbol) {
        const t = [0, 0];
        for (let dim = 0; dim < 2; dim++)
            if (isFinite(this.coordMin[dim]) && isFinite(this.coordMax[dim]))
                // center symbol on main symbol's center
                t[dim] = (this.coordMin[dim] + this.coordMax[dim]
                    - symbol.coordMin[dim] - symbol.coordMax[dim]) / 2;
        symbol.translate(t);
        for (const i in symbol.strokes)
            this.push(symbol.strokes[i]);
    }

    addInside(symbol: Symbol) {
        const t = [0, 0];
        const center = [0, 0];
        for (let dim = 0; dim < 2; dim++)
            if (isFinite(this.coordMin[dim]) && isFinite(this.coordMax[dim])) {
                center[dim] = 0.5 * (this.coordMin[dim] + this.coordMax[dim]);
                // center symbol on main symbol's center
                t[dim] = center[dim] - 0.5 * (symbol.coordMin[dim] + symbol.coordMax[dim]);
            }
        symbol.translate(t);

        if (this.strokes.length) {
            // Find necessary scaling to make inner symbol `symbol`
            // fit into outer symbol `this`.
            // Compute scaling with Manhattan distance and with Chebyshev distance,
            // and take the smallest of the two scalings.
            let sMin = Infinity;
            for (let deg = 0; deg < 2; deg++) {
                // smallest distance from center to outer symbol
                // compute the distance to each stroke and take the smallest
                let outerHoleRadius = Infinity;
                for (const i in this.strokes) {
                    const d = this.strokes[i].minDist(center, deg);
                    if (d == 0) {
                        console.error('Not enough space at center of symbol '
                            + 'to put another symbol inside');
                            return;
                    }
                    if (d < outerHoleRadius)
                        outerHoleRadius = d;
                }

                // radius of inner symbol
                // compute the distance to each stroke and take the greatest
                let innerRadius = 0;
                for (const i in symbol.strokes) {
                    const d = symbol.strokes[i].maxDist(center, deg);
                    if (d > innerRadius)
                        innerRadius = d;
                }

                // innerRadius may be null, then just don't scale
                const s = innerRadius ? outerHoleRadius / innerRadius : 1;
                if (s < sMin)
                    sMin = s;
            }
            symbol.scale(sMin, center);
        }

        for (const i in symbol.strokes)
            this.push(symbol.strokes[i]);
    }

    addAtPos(symbol: Symbol, currentPos: number[]) {
        const strokes = symbol.strokes;
        if (!strokes.length)
            return;
        for (const stroke of symbol.strokes) {
            stroke.translate(currentPos);
            this.push(stroke);
        }
        const lastStroke = strokes[strokes.length - 1];
        if (lastStroke instanceof Line) {
            currentPos[0] = lastStroke.v1[0];
            currentPos[1] = lastStroke.v1[1];
        }
        else if (lastStroke instanceof Arc) {
            currentPos[0] = lastStroke.center[0] +
                lastStroke.radius * Math.cos(lastStroke.angleRange[1]);
            currentPos[1] = lastStroke.center[1] +
                lastStroke.radius * Math.sin(lastStroke.angleRange[1]);
        }
    }

    draw(ctx: CanvasRenderingContext2D, offset: number[], scale: number) {
        ctx.beginPath();
        let previousToFill = false;
        for (const stroke of this.strokes) {
            if (previousToFill && !stroke.toFill)
                ctx.fill();
            if (!stroke.toFill || !previousToFill)
                ctx.beginPath();
            stroke.draw(ctx, offset, scale);
            previousToFill = stroke.toFill;
        }
        if (previousToFill)
            ctx.fill();
    }
}