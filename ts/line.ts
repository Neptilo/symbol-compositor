import { Geom } from "./geom.js";
import { Stroke } from "./stroke.js";

export class Line extends Stroke{
    v0: number[];
    v1: number[];

    constructor(v0: number[], v1: number[]) {
        super();
        this.v0 = v0;
        this.v1 = v1;
        this.toFill = false;
    }

    draw(ctx: CanvasRenderingContext2D, offset: number[], scale: number) {
        if (offset == undefined)
            offset = [0, 0];
        if (scale == undefined)
            scale = 1;
        ctx.moveTo(this.v0[0]*scale+offset[0], this.v0[1]*scale+offset[1]);
        ctx.lineTo(this.v1[0]*scale+offset[0], this.v1[1]*scale+offset[1]);
        if (!this.toFill) ctx.stroke();
    }

    translate(t: number[]) {
        this.v0[0] += t[0];
        this.v0[1] += t[1];
        this.v1[0] += t[0];
        this.v1[1] += t[1];
    }

    getCoordMax(dim: number) {
        return Math.max(this.v0[dim], this.v1[dim]);
    }

    getCoordMin(dim: number) {
        return Math.min(this.v0[dim], this.v1[dim]);
    }

    eval(dim: number, x: number) {
        const codim = Number(!dim);
        return this.v0[dim]+
            (this.v1[dim]-this.v0[dim])/(this.v1[codim]-this.v0[codim])
            *(x-this.v0[codim]);
    }

    diff(dim: string | number, x: number) {
        const codim = Number(!dim);
        return (this.v1[dim]-this.v0[dim])/(this.v1[codim]-this.v0[codim]);
    }

    // return the distance between `pt` and the nearest point in this line
    // If deg is 0, the Chebyshev distance is used
    // If deg is 1, the Manhattan distance is used
    minDist(pt: number[], deg: number) {
        if (deg == 0) {
            const sign = ((this.v1[0] > this.v0[0]) == (this.v1[1] > this.v0[1])) ? 1 : -1;

            // change of coordinate system: x -> x + sign * y.
            // diagonal lines (x + sign * y = const)
            // become vertical (y = const)
            const ptNew = [pt[0] + sign * pt[1], pt[1]];
            const line = new Line(
                [this.v0[0] + sign * this.v0[1], this.v0[1]],
                [this.v1[0] + sign * this.v1[1], this.v1[1]]
            );
            
            if (ptNew[0] > line.getCoordMin(0) && ptNew[0] < line.getCoordMax(0))
                return Math.abs(line.eval(1, ptNew[0]) - ptNew[1]);
        } else if (deg == 1) {
            let dMin = Infinity;
            for (let dim = 0; dim < 2; dim++) {
                if (pt[dim] > this.getCoordMin(dim) && pt[dim] < this.getCoordMax(dim)) {
                    const codim = Number(!dim);
                    const d = Math.abs(this.eval(codim, pt[dim]) - pt[codim]);
                    if (d < dMin)
                        dMin = d;
                }
            }
            if (isFinite(dMin))
                return dMin;
        }

        return Math.min(Geom.dist(this.v0, pt, deg), Geom.dist(this.v1, pt, deg));
    }

    // return the distance between `pt` and the farthest point in this line
    // If deg is 0, the Chebyshev distance is used
    // If deg is 1, the Manhattan distance is used
    maxDist(pt: number[], deg: number) {
        return Math.max(Geom.dist(this.v0, pt, deg), Geom.dist(this.v1, pt, deg));
    }

    // scale symbol by factor `s`, from center `c`
    scale(s: number, c: number[]) {
        for (let dim = 0; dim < 2; dim++) {
            this.v0[dim] = c[dim] + s * (this.v0[dim] - c[dim]);
            this.v1[dim] = c[dim] + s * (this.v1[dim] - c[dim]);
        }
    }
}