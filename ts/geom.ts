export const Geom = {
    // returns the distance between two points represented as arrays
    // If deg is 0, the Chebyshev distance is calculated
    // If deg is 1, the Manhattan distance is calculated
    // If deg is 2 or undefined, the Euclidian distance is calculated
    dist(a: number[], b: number[], deg: number) {
        const dim = a.length;
        if (b.length != dim) {
            console.error('Dimension mismatch in Geom.dist');
            return 0;
        }
        if (deg == undefined)
            deg = 2;
        switch (deg) {
            case 0:
            {
                let d = 0;
                for (let i = 0; i < dim; i++)
                    d = Math.max(d, Math.abs(b[i]-a[i]));
                return d;
            }
            case 1:
            {
                let d = 0;
                for (let i = 0; i < dim; i++)
                    d += Math.abs(b[i]-a[i]);
                return d;
            }
            case 2:
                let d2 = 0;
                for (let i = 0; i < dim; i++) {
                    const diff = b[i]-a[i];
                    d2 += diff*diff;
                }
                return Math.sqrt(d2);
            default:
                console.error('Distance of degree', deg, 'not implemented');
                return 0;
        }
    }
}