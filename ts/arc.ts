import { Geom } from "./geom.js"
import { Stroke } from "./stroke.js";

export class Arc extends Stroke {
    center: number[];
    radius: number;
    quarters: number[]; // quaters bounds must be in increasing order
    angleRange: number[];
    
    constructor(center: number[], radius: number, quarters: number[]) {
        super();
        this.center = center;
        this.radius = radius;
        this.quarters = quarters.sort();
        this.angleRange = [Math.PI / 2 * quarters[0], Math.PI / 2 * quarters[1]];
        this.toFill = false;
    }

    draw(ctx: CanvasRenderingContext2D, offset: number[], scale: number) {
        if (offset == undefined)
            offset = [0, 0];
        if (scale == undefined)
            scale = 1;
        const counterClockwise = this.angleRange[0] > this.angleRange[1];
        ctx.arc(
            this.center[0] * scale + offset[0],
            this.center[1] * scale + offset[1],
            this.radius * scale,
            this.angleRange[0],
            this.angleRange[1],
            counterClockwise);
        if (!this.toFill) ctx.stroke();
    }

    translate(t: number[]) {
        this.center[0] += t[0];
        this.center[1] += t[1];
    }

    getCoordMin(dim: number) {
        if (dim == 0) {
            if (this.quarters[0] <= 2 && this.quarters[1] >= 2) {
                // The arc touches the point at the left of the circle
                // so the minimum is the x component of that point
                return this.center[dim] - this.radius;
            } else {
                return Math.min(
                    this.center[dim] + this.radius * Math.cos(this.angleRange[0]),
                    this.center[dim] + this.radius * Math.cos(this.angleRange[1]));
            }
        } else {
            if (this.quarters[0] <= 3 && this.quarters[1] >= 3) {
                // The arc touches the point at the top of the circle
                // so the minimum is the y component of that point
                return this.center[dim] - this.radius;
            } else {
                return Math.min(
                    this.center[dim] + this.radius * Math.sin(this.angleRange[0]),
                    this.center[dim] + this.radius * Math.sin(this.angleRange[1]));
            }
        }
    }

    getCoordMax(dim: number) {
        if (dim == 0) {
            if ((this.quarters[0] <= 0 && this.quarters[1] >= 0) ||
                (this.quarters[0] <= 4 && this.quarters[1] >= 4))
            {
                // The arc touches the point at the right of the circle
                // so the maximum is the x component of that point
                return this.center[dim] + this.radius;
            } else {
                return Math.max(
                    this.center[dim] + this.radius * Math.cos(this.angleRange[0]),
                    this.center[dim] + this.radius * Math.cos(this.angleRange[1]));
            }
        } else {
            if (this.quarters[0] <= 1 && this.quarters[1] >= 1) {
                // The arc touches the point at the bottom of the circle
                // so the maximum is the y component of that point
                return this.center[dim] + this.radius;
            } else {
                return Math.max(
                    this.center[dim] + this.radius * Math.sin(this.angleRange[0]),
                    this.center[dim] + this.radius * Math.sin(this.angleRange[1]));
            }
        }
    }

    // The arc must represent a function (with a single image for each point)
    // in the specified dimension.
    eval(dim: number, x: number) {
        const codim = Number(!dim);
        const dx = x - this.center[codim];

        // compute sign to apply to the square root
        const middleQuarter = (this.quarters[1] + this.quarters[0]) / 2;
        let sign: number;
        if ((dim == 0 && (middleQuarter <= 1 || middleQuarter >= 3)) ||
            (dim == 1 && middleQuarter <= 2 && middleQuarter >= 0)) {
            sign = 1;
        } else {
            sign = -1;
        }

        return this.center[dim] + sign * Math.sqrt(this.radius * this.radius - dx * dx);
    }

    // The arc must represent a function (with a single image for each point)
    // in the specified dimension.
    diff(dim: number, x: number) {
        const codim = Number(!dim);
        const dx = x - this.center[codim];

        // compute sign to apply to the square root
        const middleQuarter = (this.quarters[1] + this.quarters[0]) / 2;
        let sign: number;
        if ((dim == 0 && (middleQuarter <= 1 || middleQuarter >= 3)) ||
            (dim == 1 && middleQuarter <= 2 && middleQuarter >= 0)) {
            sign = 1;
        } else {
            sign = -1;
        }

        return -sign * dx / Math.sqrt(this.radius * this.radius - dx * dx);
    }

    split(dim: number): Stroke[] {
        const strokes: Stroke[] = [];
        if (dim == 0) {
            const crossesQuarter1 =
                this.quarters[0] < 1 && this.quarters[1] > 1;
            const crossesQuarter3 =
                this.quarters[0] < 3 && this.quarters[1] > 3;
            if (crossesQuarter1) {
                strokes.push(new Arc(
                    this.center, this.radius, [this.quarters[0], 1]));
                if (crossesQuarter3) {
                    strokes.push(new Arc(
                        this.center, this.radius, [1, 3]));
                } else {
                    strokes.push(new Arc(
                        this.center, this.radius, [1, this.quarters[1]]));
                }
            } else {
                if (crossesQuarter3) {
                    strokes.push(new Arc(
                        this.center, this.radius, [this.quarters[0], 3]));
                } else {
                    strokes.push(this);
                }
            }
            if (crossesQuarter3) {
                strokes.push(new Arc(
                    this.center, this.radius, [3, this.quarters[1]]));
            }
        } else {
            if (this.quarters[0] < 2 &&
                this.quarters[1] > 2) {
                strokes.push(new Arc(
                    this.center, this.radius, [this.quarters[0], 2]));
                strokes.push(new Arc(
                    this.center, this.radius, [2, this.quarters[1]]));
            } else {
                strokes.push(this);
            }
        }
        return strokes;
    }

    // return the distance between `pt` and the nearest point in this arc
    // If deg is 0, the Chebyshev distance is used
    // If deg is 1, the Manhattan distance is used
    minDist(pt: number[], deg: number) {
        let dMin = Infinity;
        // sample points every eigth of a circle
        for (let q = this.quarters[0]; q <= this.quarters[1]; q += 0.5) {
            const angle = Math.PI * 0.5 * q;
            const samplePt = [
                this.center[0] + this.radius * Math.cos(angle),
                this.center[1] + this.radius * Math.sin(angle)
            ];
            const d = Geom.dist(pt, samplePt, deg);
            if (d < dMin)
                dMin = d;
        }
        return dMin;
    }

    // return the distance between `pt` and the farthest point in this arc
    // If deg is 0, the Chebyshev distance is used
    // If deg is 1, the Manhattan distance is used
    maxDist(pt: number[], deg: number) {
        let dMax = 0;
        // sample points every eigth of a circle
        for (let q = this.quarters[0]; q <= this.quarters[1]; q += 0.5) {
            const angle = Math.PI * 0.5 * q;
            const samplePt = [
                this.center[0] + this.radius * Math.cos(angle),
                this.center[1] + this.radius * Math.sin(angle)
            ];
            const d = Geom.dist(pt, samplePt, deg);
            if (d > dMax)
                dMax = d;
        }
        return dMax;
    }

    // scale symbol by factor `s`, from center `c`
    scale(s: number, c: number[]) {
        for (let dim = 0; dim < 2; dim++)
            this.center[dim] = c[dim] + s * (this.center[dim] - c[dim]);
        this.radius *= s;
    }
}