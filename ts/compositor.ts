import { Arc } from './arc.js';
import { Symbol } from './symbol.js';
import { Dot } from './dot.js';
import { Line } from './line.js';

function updateSymbol() {
    const string = (<HTMLInputElement>document.getElementById('string')).value;
    const w = canvas.width;
    const h = canvas.height;
    const symbol = getSymbol(string);
    ctx.clearRect(0, 0, w, h);
    let hScale: number, vScale: number, scale: number, offset: number[];
    if (symbol.getWidth())
        hScale = (w - ctx.lineWidth) / symbol.getWidth();
    if (symbol.getHeight())
        vScale = (h - ctx.lineWidth) / symbol.getHeight();
    if (vScale == undefined && hScale == undefined) {
        scale = 1;
        offset = [
            (w - symbol.coordMin[0]) / 2,
            (h - symbol.coordMin[1]) / 2
        ];
    } else if (vScale == undefined || hScale <= vScale) {
        scale = hScale;
        offset = [
            ctx.lineWidth / 2 - scale * symbol.coordMin[0],
            (h - scale * (symbol.coordMin[1] + symbol.coordMax[1])) / 2
        ];
    } else {
        scale = vScale;
        offset = [
            (w - scale * (symbol.coordMin[0] + symbol.coordMax[0])) / 2,
            ctx.lineWidth / 2 - scale * symbol.coordMin[1]
        ];
    }
    symbol.draw(ctx, offset, scale);
}

function getSymbol(string: string) {
    let symbol = new Symbol();
    if (!string)
        return symbol;

    switch (string[0]) {
        case 'l':
        case 'L':
        {
            if (string.length < 2) {
                console.error("Missing digit after 'l'/'L'");
                return symbol;
            }
            const dirInd = parseInt(string[1]);
            const angle = Math.PI / 4 * dirInd;
            symbol.push(new Line([0, 0], [Math.cos(angle), Math.sin(angle)]));
            break;
        }
        case '-':
            symbol.push(new Line([0, 0], [1, 0]));
            break;
        case '|':
        case 'I':
            symbol.push(new Line([0, 0], [0, 1]));
            break;
        // lines forming a right angle
        case 'A': // clockwise
        case 'a': // counter-clockwise
        {
            if (string.length < 2) {
                console.error("Missing digit after 'a'/'A'");
                return symbol;
            }
            let dirInd = parseInt(string[1]); // possibly out of bounds
            let angle = Math.PI / 4 * dirInd;
            const l1 = new Line([0, 0], [Math.cos(angle), Math.sin(angle)]);
            dirInd = string[0] == 'A' ? dirInd + 2 : dirInd - 2;
            angle = Math.PI / 4 * dirInd;
            const l2 = new Line([0, 0], [Math.cos(angle), Math.sin(angle)]);
            l2.translate(l1.v1);
            symbol.push(l1);
            symbol.push(l2);
            break;
        }
        // quarter-circle
        case 'Q': // clockwise (= trigonometric because y is pointing down)
        case 'q': // counter-clockwise
        {
            if (string.length < 2) {
                console.error("Missing digit after 'q'/'Q'");
                return symbol;
            }
            const dirInd = parseInt(string[1]);
            const startQuarter = dirInd / 2;
            const endQuarter = string[0] == 'Q' ? startQuarter + 1 :
                                                  startQuarter - 1;
            const arc = new Arc([0, 0], 1, [startQuarter, endQuarter]);
            arc.translate([
                -Math.cos(arc.angleRange[0]),
                -Math.sin(arc.angleRange[0])]);
            symbol.push(arc);
            break;
        }
        // half-circle
        case 'H': // clockwise
        case 'h': // counter-clockwise
        {
            if (string.length < 2) {
                console.error("Missing digit after 'h'/'H'");
                return symbol;
            }
            const dirInd = parseInt(string[1]);
            const startQuarter = dirInd / 2;
            const endQuarter = string[0] == 'H' ? startQuarter + 2 :
                                                  startQuarter - 2;
            const arc = new Arc([0, 0], 1, [startQuarter, endQuarter]);
            arc.translate([
                -Math.cos(arc.angleRange[0]),
                -Math.sin(arc.angleRange[0])]);
            symbol.push(arc);
            break;
        }
        case '[':
            symbol.push(new Line([0, 0], [1, 0]));
            symbol.push(new Line([0, 0], [0, 1]));
            symbol.push(new Line([0, 1], [1, 1]));
            break;
        case ']':
            symbol.push(new Line([0, 0], [1, 0]));
            symbol.push(new Line([1, 0], [1, 1]));
            symbol.push(new Line([0, 1], [1, 1]));
            break;
        case '+':
            symbol.push(new Line([0, 1], [2, 1]));
            symbol.push(new Line([1, 0], [1, 2]));
            break;
        case '/':
            symbol.push(new Line([0, 1], [1, 0]));
            break;
        case '\\':
            symbol.push(new Line([0, 0], [1, 1]));
            break;
        case '<':
            symbol.push(new Line([0, 1], [1, 0]));
            symbol.push(new Line([0, 1], [1, 2]));
            break;
        case '>':
            symbol.push(new Line([0, 0], [1, 1]));
            symbol.push(new Line([1, 1], [0, 2]));
            break;
        case '^':
            symbol.push(new Line([0, 1], [1, 0]));
            symbol.push(new Line([1, 0], [2, 1]));
            break;
        case 'v':
        case 'V':
            symbol.push(new Line([0, 0], [1, 1]));
            symbol.push(new Line([1, 1], [2, 0]));
            break;
        case 'X':
        case 'x':
            symbol.push(new Line([0, 1], [1, 0]));
            symbol.push(new Line([0, 0], [1, 1]));
            break;
        case '2':
            symbol.push(new Line([0, 0], [0, 1]));
            symbol.push(new Line([0, 1], [1, 1]));
            symbol.push(new Line([1, 1], [1, 0]));
            symbol.push(new Line([1, 0], [0, 0]));
            break;
        case 'o':
            symbol.push(new Arc([0, 0], 0.5, [0, 2]));
            symbol.push(new Arc([0, 0], 0.5, [2, 4]));
            break;
        case 'O':
            symbol.push(new Arc([0, 0], 1, [0, 2]));
            symbol.push(new Arc([0, 0], 1, [2, 4]));
            break;
        case '.':
            symbol.push(new Dot([0, 0]));
            symbol.nextSpacing = 1;
            break;
        case 'b':
            symbol.nextSpacing = 1;
            break;
        case '=':
        case ';':
        {    
            const subStrings = parse(string.substring(1));
            for (const i in subStrings)
                symbol.addAfter(getSymbol(subStrings[i]), 1);
            break;
        }
        case '"':
        case ',':
        {    
            const subStrings = parse(string.substring(1));
            for (const i in subStrings)
                symbol.addAfter(getSymbol(subStrings[i]), 0);
            break;
        }
        case '#':
        {    
            const subStrings = parse(string.substring(1));
            for (const i in subStrings)
                symbol.addOver(getSymbol(subStrings[i]));
            break;
        }
        case '@':
        {    
            const subStrings = parse(string.substring(1));
            for (const i in subStrings)
                symbol.addInside(getSymbol(subStrings[i]));
            break;
        }
        // continuous composition mode:
        // start each stroke at the pen postion of the last stroke
        case '&':
        {    
            const subStrings = parse(string.substring(1));
            const penPos = [0, 0];
            for (const i in subStrings)
                symbol.addAtPos(getSymbol(subStrings[i]), penPos);
            break;
        }
        case '*': // filling modifier
        {    
            symbol = getSymbol(string.substring(1));
            for (const stroke of symbol.strokes)
                stroke.toFill = true;
            break;
        }
        case '(':
        {    
            symbol = getSymbol(parse(string)[0]);
            break;
        }
    }
    return symbol;
}

function parse(string: string) {
    const res = [];
    let cur = 0;
    let start: number;
    let parNum = 0;
    while (cur < string.length) {
        switch (string[cur]) {
            case '(':
                if (!parNum)
                    start = cur + 1;
                parNum++;
                break;
            case ')':
                parNum--;
                if (!parNum)
                    res.push(string.substring(start, cur));
                break;
            case 'l':
            case 'L':
            case 'q':
            case 'Q':
            case 'h':
            case 'H':
            case 'A':
            case 'a':
                if (!parNum)
                    res.push(string[cur++] + string[cur]);
                break;
            default:
                if (!parNum)
                    res.push(string[cur]);
        }
        cur++
    }
    return res;
}

const canvas = document.getElementsByTagName('canvas')[0];
const ctx = canvas.getContext('2d');
ctx.lineWidth = 0.08 * 300;
ctx.lineCap = 'round';
(window as any).updateSymbol = updateSymbol;
