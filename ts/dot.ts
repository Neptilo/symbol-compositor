import { Geom } from "./geom.js";
import { Stroke } from "./stroke.js";

export class Dot extends Stroke {
    position: number[];

    constructor(position: number[]) {
        super();
        this.position = position;
    }

    draw(ctx: CanvasRenderingContext2D, offset: number[], scale: number) {
        if (offset == undefined)
            offset = [0, 0];
        if (scale == undefined)
            scale = 1;
        ctx.arc(
            this.position[0] * scale + offset[0],
            this.position[1] * scale + offset[1],
            ctx.lineWidth / 2,
            0,
            2 * Math.PI);
        ctx.fill();
    }

    translate(t: number[]) {
        this.position[0] += t[0];
        this.position[1] += t[1];
    }

    getCoordMax(dim: number) {
        return this.position[dim];
    }

    getCoordMin(dim: number) {
        return this.position[dim];
    }

    eval(dim: number, x: number) {
        return this.position[dim];
    }

    diff(dim: number, x: number): number {
        throw new Error("You can't differentiate a dot.");
    }

    // return the distance between `pt` and the dot
    // If deg is 0, the Chebyshev distance is used
    // If deg is 1, the Manhattan distance is used
    maxDist(pt: number[], deg: number) {
        return Geom.dist(pt, this.position, deg);
    };
    minDist(pt: number[], deg: number) {
        return this.maxDist(pt, deg);
    }

    // scale symbol by factor `s`, from center `c`
    scale(s: number, c: number[]) {
        for (let dim = 0; dim < 2; dim++)
            this.position[dim] = c[dim] + s * (this.position[dim] - c[dim]);
    }
}