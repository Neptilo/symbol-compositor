Installation
============

* Install Node.js
* `npm update`
* `npx tsc`

Running the application
=======================

* For example, if you have Python installed, `python -m http.server 8000`, or `npx http-server` otherwise.
* Open http://localhost:8000/symbol.html in a browser

Usage
=====

Type a code in the text field to generate a symbol.

A symbol is defined recursively as follows:

* A *symbol* is either a *shape* or an operator followed by a sequence of *symbols* and *spaces*. To separate *symbols* from one another, they have to be surrounded by parentheses.
* The operators are the *vertical*, *horizontal*, *overlapping* and *concentrical structurers*, the *continuous compositor* and the *filling modifier*.
* The *vertical structurer*, symbolized as '=' or ';', tells the following symbols to be drawn from top to bottom, aligned and stuck together, unless a space between two symbols tells otherwise.
* The *horizontal structurer*, symbolized as '"' or ',', tells the following symbols to be drawn from left to right, aligned and stuck together, unless a space between two symbols tells otherwise.
* The *overlapping structurer*, symbolized as '#', tells the following symbols to be drawn at the same place, overlapping each other.
* The *concentrical structurer*, symbolized as '@', tells the following symbols to be drawn from outside to inside, sharing the same center, and scaled so as to touch each other without overlapping (more or less).
* The *continuous compositor*, symbolized as '&', tells the following symbols to be drawn by starting each stroke at the end position of the last stroke. Those symbols must all be *strokes*.
* The *filling modifier*, symbolized by '*', tells the inside of the shape drawn by the following symbols to be filled instead of being outlined.
* A *shape* is a *stroke* or a *common shape*.
* A *stroke* is a *straight line*, an *arc* or a *dot*.
* A *stright line* is symbolized by 'l' or 'L', followed by a digit indicating the direction. Alternatively, a horizontal line can be symbolized by '-', a vertical one by 'I' or '|', and a diagonal one by '/' or '\', obviously indicating in which direction the diagonal goes.
* An *arc* is either a quarter of a circle, symbolized by 'Q' or 'q', or a half-circle, symbolized by 'H' or 'h'. In all cases, the letter is followed by a digit indicating the direction from the center of the circle to the starting point of the arc. 'Q' and 'H' are drawn clockwise, while 'q' and 'h' are drawn counter-clockwise.
* A *common shape* is a *chevron*, a *bracket*, a *cross*, a *square* or a *circle*.
* A *chevron* is a group of two straight lines forming a right angle, symbolized by 'a' or 'A', followed by a digit indicating the direction of the first drawn line. Considering the line's starting point as the center of the drawing, the second line is drawn clockwise for 'a', or counter-clockwise for 'A'. Alternatively, '<', '>', '^', 'v' and 'V' also define chevrons, each in obvious directions.
* A *bracket* is a group of three straight lines forming two right angles in the same direction. They are symbolized by '[' or ']'.
* A *cross* is a group of two orthogonal straight lines crossing at their middle. They are symbolized as '+' or 'x' or 'X'.
* A *square* is... Well, you know what a square is! It is symbolized as '2'.
* A *circle* is symbolized as 'o' or 'O', the latter being twice as big.
* A *dot* is symbolized as '.'.
* A *space* is symbolized as 'b' (like "blank").

As previsously mentioned, symbols 'l', 'L', 'q', 'Q', 'h', 'H', 'A', 'a' expect a digit after them to specify the direction of the stroke.

Here is a diagram associating each direction, starting from the center, with a digit:

```
5 6 7
 \|/
4-+-0
 /|\
3 2 1
```

The `+` at the center reprensents the starting point and the numbers represent the possible end points.

**Examples:**
```
&Q6L4h0L4Q4H4
&q6l0H4l0q0h0
"A6(*&Q4H4)
```